package br.com.galgo.testes.suite;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

//@RunWith(StopOnFirstFailureSuite.class)
/*
@Suite.SuiteClasses({//
        TesteConsultaEntidade.class,//
        TesteConsultaCompromisso.class,//
        TesteConsultaLog.class,//
        TesteConsultaPLCota.class,//
        TesteConsultaFundos.class, //
        TesteFundosWS.class,//
        TestePLCotaWS.class, //
        TesteConectividadeWS.class //
})
*/
public class SuiteAplicacao {

    private static final String PASTA_SUITE = "Aplicação";

    //    @BeforeClass
    public static void setUp() throws Exception {
        SuiteUtils.configurarSuiteDefault(Ambiente.PRODUCAO, PASTA_SUITE);
    }

}
