package br.com.galgo.testes.suite;

/**
 * Created by valdemar.arantes on 05/06/2015.
 */
/*
public class SuiteListener implements ISuiteListener {

    private static final Logger log = LoggerFactory.getLogger(SuiteListener.class);

    @BeforeSuite
    public void beforeSuite() {
        log.debug("Antes da suíte");
    }

    */
/**
     * This method is invoked before the SuiteRunner starts.
     *
     * @param suite
 *//*

    public void onStart(ISuite suite) {
        log.debug("Iniciando a suíte {}", suite.getName());
        SuiteUtils.configurarSuiteDefault(Ambiente.PRODUCAO, "Aplicação");
    }

    */
/**
     * This method is invoked after the SuiteRunner has run all
     * the test suites.
     *
     * @param suite
 *//*

    public void onFinish(ISuite suite) {
        log.debug("onFinish");
    }
}
*/

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.utils.SuiteAplicacaoListener;

/**
 * Created by valdemar.arantes on 05/06/2015.
 */
public class SuiteListener extends SuiteAplicacaoListener {

    @Override
    protected Ambiente getAmbiente() {
        return Ambiente.PRODUCAO;
    }

}

